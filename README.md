# Budgetocratie

Time spent so far : 5h09/24h

## Planning

1. [x] Définition du projet
2. [x] Recherche des règles business
3. [x] Recherche des règles business
4. [x] Initialisation du projet
5. [x] Design
6. [x] Design
7. [x] Estimation et création de la base de données
8. [x] Estimation et création de la base de données
9. Réalisation technique
10. Réalisation technique
11. Réalisation technique
12. Point avancement (avec redéfinition)
13.
14.
15.
16.
17.
18. Point avancement (avec redéfinition)
19.
20.
21.
22.
23.
24. Point de fin !

## Objectif du jeu de gestion

Vous êtes un candidat à la prochaine élection présidentielle. Il vous faut moduler le budget annuel de la france afin de satisfaire les demandes du peuple.

## Objectif du projet

Faire prendre conscience de la complexité de faire un budget et qu'une augmentation dans tous les domaines n'est pas possible.

## Contraintes

* Temps : 24h dépensées.
* Stack : HTML, CSS, JS
* Framework : Chart.js

## MVP

* En tant qu'utilisateur, je peux accepter une proposition
* En tant qu'utilisateur, je peux visualiser le résumé de mon budget
* En tant qu'utilisateur, je peux visualiser le détail de mon budget

## Indicateurs de réussite

* Pourcentage de la population satisfaite
* Viabilité économique du budget
* Influence internationale : Hard power + Soft power

## Catégories

* Environnement
* Economie - Emplois, impôts, taxes
* Education
* Santé
* Social
* Sécurité

## Ressources

* [economie.gouv.fr](ttps://www.economie.gouv.fr/facileco/comptes-publics/budget-etat)
* [budget.gouv.fr](https://www.budget.gouv.fr/)

## Interface

à gauche, un tableau de bord qui regroupe l'ensemble des dépenses selon la catégorie (Cf budget de l'état)

à droite la liste des propositions chiffrées, que l'on peut accepter ou refuser.

## Remerciements

* [mavadee](https://www.flaticon.com/authors/mavadee)
* https://www.flaticon.com/authors/iconjam
* https://www.flaticon.com/free-icons/click
* https://www.flaticon.com/free-icons/euro