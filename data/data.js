const Categories = {
    Environment: "Environnement",
    Economy: "Economie",
    Education: "Education",
    Health: "Santé",
    Social: "Social",
    Security: "Sécurité"
}
/**
 * data structure:
 * @type {{
 *     articleContent: string
 *     title: string
 *     category: string,
 *     effect: {
 *         satisfaction: number,
 *         influence: number
 *     },
 *     cost: number,
 *     selected: boolean
 * }}
 *
 *
 *
 */
{
    simplePropositions: [
        {
            articleContent: "La taxe d’habitation est un impôt foncier payé aux collectivités territoriales par toute personne propriétaire ou locataire du logement qu’il occupe. Elle est établie selon la valeur locative du logement et de ses dépendances. Cette proposition vise à exonérer les classes moyennes et populaire de cette taxe, soit pour 80% de la population.",
            title: "Exonérer 80% des ménages de la taxe d'habitation",
            category: Categories.Economy,
            effect: {
                satisfaction: 5,
                influence: 0
            },
            cost: 17.6,
            selected: false
        },
        {
            articleContent: "En France, 6,5 millions de foyers n'ont pas accès à un bon débit internet. Le haut débit dont il est question s’avère être le réseau 5G, qui a concentré de vives polémiques entre les écologistes favorables à une sobriété énergétique généralisée, et les défenseurs des avancées technologiques nécessaires au maintien d’une concurrence forte.",
            title: "Couvrir l’ensemble du territoire avec un réseau de haut débit",
            category: Categories.Social,
            effect: {
                satisfaction: 8,
                influence: 2
            },
            cost: 19,
            selected: false
        },
        {
            articleContent: "Selon l’Observatoire International des Prisons, en 2016 le taux d’occupation des établissements pénitenciers français atteint les 116%. Cette surpopulation se concentre dans les maisons d’arrêts. Le candidat propose donc de créer 15 000 places supplémentaire,  ce qui revient à augmenter de 25% les capacités des prisons en 2021.",
            title: "Construire 15 000 nouvelles places de prison",
            category: Categories.Security,
            effect: {
                satisfaction: 2,
                influence: 0
            },
            cost: 0.45,
            selected: false
        },
        {
            articleContent: "Investissement de plus de 30 milliards d’euros dans les champions industriels français des “objets du quotidien” hors des domaines d’excellence français (comme le luxe ou l’aéronautique). Il s’agit d’investir dans des secteurs comme les petits réacteurs nucléaires, l'hydrogène vert, les véhicules électriques, les avions bas-carbone et plusieurs autres branches du secteur énergétique. Le candidat entend ainsi “bâtir la france de 2030 et faire émerger dans notre pays et en Europe les champions de demain”.",
            title: "Investir 30 millards d’euros dans les champions industriels français",
            category: Categories.Economy,
            effect: {
                satisfaction: 4,
                influence: 6
            },
            cost: 30,
            selected: false

        },
        {
            articleContent: "Cette proposition vise à diminuer les cotisations, les taxes et les impôts payés par les entreprises. A long terme, le but escompté est une réindustrialisation de la France via un accroissement de la compétitivité des entreprises.",
            title: "Baisser les impôts sur la production",
            category: Categories.Economy,
            effect: {
                satisfaction: 2,
                influence: 4
            },
            cost: 10,
            selected: false
        },
        {
            articleContent: "Cette mesure se traduirait par la création de 1 500 « cyberpatrouilleurs » ainsi qu’une école au sein du ministère de l’intérieur formant les agents de l’ordre public sur cette thématique. La dépense pour contribuer à cette formation serait d’un milliard d’euros. Enfin, un équivalent de l’appel du numéro 17 de la police secours serait mis en place permettant à chaque citoyen de signaler une cyberattaque.",
            title: "Créer une formation cyber pour lutter contre les attaques numériques",
            category: Categories.Security,
            effect: {
                satisfaction: 0,
                influence: 2
            },
            cost: 0.05,
            selected: false

        },
        {
            articleContent: "Il est souvent dit que l’énergie la plus verte est celle que l’on ne consomme pas. En suivant cette logique l’un des moyens d’action les plus efficaces pour limiter nos dépenses énergétiques, et par extension nos émissions de CO2, consiste à mieux isoler thermiquement la vaste majorité des bâtiments dont la faible isolation force mécaniquement à dépenser plus d’énergie pour les maintenir au chaud, ce qui émet de CO2 plus que nécessaire.",
            title: "Rénover 1 million de logements mal isolés",
            category: Categories.Environment,
            effect: {
                satisfaction: 3,
                influence: 1
            },
            cost: 2.8,
            selected: false
        },
        {
            articleContent: "Il s’agirait d’un service obligatoire de 3 à 6 mois, plus civique que militaire, qui remplacerait la journée défense et citoyenneté. Son objectif est la création d’une cohésion nationale de toutes les classe sociales au moyen d’une camaraderie formée autour des valeurs républicaines.",
            title: "Instaurer un service national universel",
            category: Categories.Social,
            effect: {
                satisfaction: -4,
                influence: 0
            },
            cost: 5,
            selected: false

        },
        {
            articleContent: "Dans le cadre de cette proposition les enseignants mutés dans ces zones devront avoir 3 ans d’ancienneté et recevront une prime annuelle de 3000 euros nets. Il existe aujourd’hui 12 000 classes de CP et de CE1 en zone prioritaire.",
            title: "Limiter à 12 élèves par enseignant les classes de CP et de CE1 en zone prioritaire",
            category: Categories.Education,
            effect: {
                satisfaction: 3,
                influence: 1
            },
            cost: 0.51,
            selected: false
        },
        {
            articleContent: "Cette mesure envisage principalement de rehausser le montant des allocations chômage pour les intermittents du spectacle, actuellement de 38 euros bruts par jour minimum pour les ouvriers et techniciens du spectacle, et 44 euros bruts par jour minimum pour les artistes du spectacle.",
            title: "Empêcher la précarisation des artistes et des intermittents du spectacle",
            category: Categories.Social,
            effect: {
                satisfaction: 1,
                influence: 1
            },
            cost: 1,
            selected: false
        },
        {
            articleContent: "Le montant du salaire minimum interprofessionnel de croissance (SMIC) s’élève aujourd’hui à 1 603,12 € brut mensuel pour un temps plein. Le candidat souhaite l’augmenter de 15% (soit 200€ net de plus par mois) dans le but d’améliorer le pouvoir d’achat des Français. La mise en place de cette augmentation sera permise à travers un dialogue entre les partenaires sociaux (c’est-à-dire, une discussion entre organisations patronales et syndicales).",
            title: "Augmenter le montant du SMIC de 15%",
            category: Categories.Social,
            effect: {
                satisfaction: 10,
                influence: 0
            },
            cost: 15,
            selected: false
        },
        {
            articleContent: "L’agroécologie est fondée sur le maintien des cycles de fertilité et de l’équilibre entre élevage et culture. Ce modèle de production doit permettre un meilleur revenu pour les producteurs, la valorisation de la qualité des produits et du bien-être animal ainsi que l’arrêt de l’usage d’engrais de synthèse et de pesticides. Ces objectifs seront atteints notamment grâce à la formation des agriculteurs.",
            title: "Promouvoir l’agroécologie",
            category: Categories.Environment,
            effect: {
                satisfaction: 3,
                influence: 2
            },
            cost: 1,
            selected: false
        },
        {
            articleContent: "Afin de mettre à contribution les plus fortunés pour financer la transition énergétique, un Impôt de Sol",
            title: "Mettre en place un impôt sur la fortune climatique",
            category: Categories.Environment,
            effect: {
                satisfaction: 6,
                influence: 0
            },
            cost: -4.3,
            selected: false
        },
        {
            articleContent: "Le candidat souhaite qu’une plus grande partie du budget français soit allouée à la culture qu’il estime primordial avec, notamment, la restauration de nombreux musées nationaux, gratuits pour les moins de 25 ans ainsi que la construction de nombreux sites culturels accessibles sur tous les territoires.",
            title: "Augmenter considérablement le budget de la culture",
            category: Categories.Social,
            effect: {
                satisfaction: 4,
                influence: 0
            },
            cost: 0.273,
            selected: false
        },
        {
            articleContent: "Afin de lutter contre le manque de personnel médical, jusque 15 000 nouveaux médecins seront formés chaque année, contre moins de 10 000 aujourd’hui. Les formations de soignants seront portées à 1 250 sages-femmes par an, 25 000 infirmiers et aides-soignants, 5 000 logisticiens, techniciens et agents hospitaliers. Une revalorisation des rémunérations et des carrières médicales sera effectuée afin de rentre ces professions plus attractives.",
            title: "Accroître les capacités d’accueil des facultés de médecine",
            category: Categories.Health,
            effect: {
                satisfaction: 3,
                influence: 0
            },
            cost: 0.389,
            selected: false

        },
        {
            articleContent: "Le budget de l'État correspond à l'ensemble de ses ressources et de ses dépenses. Il s’élève à plusieurs dizaines de milliards d’euros.",
            title: "Consacrer 1% du budget de l’Etat à la culture",
            category: Categories.Social,
            effect: {
                satisfaction: 3,
                influence: 0
            },
            cost: 10,
            selected: false
        },
        {
            articleContent: "Selon les données du ministère de l'Éducation nationale, 1 162 850 personnes travaillaient en 2020 dans les établissements scolaires. Parmi eux, on comptait environ 866 000 enseignants. Dans beaucoup d’établissements, on observe des postes vacants.",
            title: "Instaurer 35 000 nouveaux postes d’enseignants et de personnels éducatifs",
            category: Categories.Education,
            effect: {
                satisfaction: 5,
                influence: 0
            },
            cost: 1.6,
            selected: false
        },
        {
            articleContent: "La crise sanitaire a mis en exergue le manque de moyens alloués à l’hôpital public. Cette mesure vise à combler ces manques.",
            title: "Embaucher 100.000 nouvelles infirmières et infirmiers",
            category: Categories.Health,
            effect: {
                satisfaction: 5,
                influence: 0
            },
            cost: 4.8,
            selected: false
        },
        {
            articleContent: "Cette proposition modifierait le montant du SMIC, de 1258,25€ actuellement à 1400€ par mois, de façon à augmenter le pouvoir d’achat des français.",
            title: "Fixer le SMIC mensuel à 1400 euros nets",
            category: Categories.Social,
            effect: {
                satisfaction: 10,
                influence: 0
            },
            cost: 10,
            selected: false
        },
]
}